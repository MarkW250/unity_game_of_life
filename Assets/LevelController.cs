﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    /* Editor exposed properties */
    [Tooltip("Grid dimension for level (level will be x by x size where x is this value")]
    [SerializeField]
    private int _gridSize = 100;

    [Tooltip("Grid dimension for level (level will be x by x size where x is this value")]
    [SerializeField]
    private float _updateRate = 2.0f;       // update rate in updates per second

    [Tooltip("Grid dimension for level (level will be x by x size where x is this value")]
    [SerializeField]
    private Block _blockPrefab = null;

    [Tooltip("Sprite to use for outline of grid")]
    [SerializeField]
    private Sprite _gridOutlineSprite = null;

    [Tooltip("Start Button Color")]
    [SerializeField]
    private Color _startButtonColor = Color.green;

    [Tooltip("Stop Button Color")]
    [SerializeField]
    private Color _stopButtonColor = Color.red;

    [Tooltip("Step Counter Text")]
    [SerializeField]
    private Text _stepCounterText = null;


    /* Private variables */
    private Block[,] _blockGrid;
    private BlockState[,] _blockGridBackup;      // used to backup state of grid before playing simulation

    private bool _playing = false;          // if we are playing game of life now or not
    private float _updateTimer = 0.0f;

    private float _maxUpdateRate = 30.0f;
    private float _minUpdateRate = 1.0f;

    private int _stepCntr = 0;

    private Color _selectedColor = Color.white;

    private GameObject _gridOutLine = null;

    private Button _stopStartButton = null;

    /* Public GUI handling methods */
    public void OnClearButton()
    {
        // clear all blocks
        clearGrid();

        // reset step count
        setStepCntr(0);
    }

    public void OnSpeedSliderUpdate(Slider slider)
    {
        // update game update rate
        _updateRate = slider.value;
        if (_updateRate < _minUpdateRate) { _updateRate = _minUpdateRate; }
        if (_updateRate > _maxUpdateRate) { _updateRate = _maxUpdateRate; }
        string speedText = string.Format("Speed: {0}", _updateRate);
        slider.gameObject.GetComponentInChildren<Text>().text = speedText;
    }

    public void OnResetGridButton()
    {
        resetGrid();
    }

    public void OnColorSelect(Button sender)
    {
        _selectedColor = sender.colors.normalColor;
    }

    public void OnStartStop(Button sender)
    {
        // this is a bit of a hack but we grab the stop start button when it is pressed.
        _stopStartButton = sender;
        setPlayState(!_playing);
    }

    // Start is called before the first frame update
    void Start()
    {
        if (_blockPrefab == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a _blockPrefab assigned!");
        }

        if (_gridOutlineSprite == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a _gridOutlineSprite assigned!");
        }

        int gridXCount = _gridSize;
        int gridYCount = _gridSize;

        _blockGrid = new Block[gridXCount, gridYCount];
        _blockGridBackup = new BlockState[gridXCount, gridYCount];

        // calculate where left most block and top most block should spawn
        float blockSize_x = _blockPrefab.transform.lossyScale.x;
        float blockSize_y = _blockPrefab.transform.lossyScale.y;
        float leftSide = -blockSize_x * ((gridXCount - 1) / 2.0f);
        float topSide = blockSize_y * ((gridYCount - 1) / 2.0f);

        // spawn the blocks
        for (int row = 0; row < gridYCount; row++)
        {
            float ypos = topSide - (row * blockSize_y);
            for (int col = 0; col < gridXCount; col++)
            {
                float xpos = leftSide + (col * blockSize_x);
                _blockGrid[col, row] = GameObject.Instantiate(_blockPrefab, new Vector2(xpos, ypos), new Quaternion());
                _blockGrid[col, row].OnClicked += this.BlockClickHandler;
            }
        }

        createGrid();
    }

    private void createGrid()
    {
        int gridXCount = _gridSize;
        int gridYCount = _gridSize;
        float blockSize_x = _blockPrefab.transform.lossyScale.x;
        float blockSize_y = _blockPrefab.transform.lossyScale.y;
        float leftSide = -blockSize_x * ((gridXCount) / 2.0f);
        float topSide = blockSize_y * ((gridYCount) / 2.0f);

        // create grid outline
        _gridOutLine = new GameObject("GridOutline");
        _gridOutLine.transform.position = new Vector2(0, 0);

        createGridSide(new Vector2(leftSide, (topSide + blockSize_y)), new Vector2(-0.5f, -0.5f), new Vector2(1, (topSide + blockSize_y) * 2), "GridSideLeft");
        createGridSide(new Vector2(leftSide, topSide), new Vector2(-0.5f, 0.5f), new Vector2(leftSide * 2, 1), "GridSideTop");
        createGridSide(new Vector2(-leftSide, (topSide + blockSize_y)), new Vector2(0.5f, -0.5f), new Vector2(1, (topSide + blockSize_y) * 2), "GridSideRight");
        createGridSide(new Vector2(leftSide, -topSide), new Vector2(0.5f, -0.5f), new Vector2(-leftSide * 2, 1), "GridSideBottom");
    }

    private void createGridSide(Vector2 baseLocation, Vector2 localLocation, Vector2 scale, string sideName = "GrideSide")
    {
        GameObject gridOutLineLeft = new GameObject(sideName);
        gridOutLineLeft.transform.SetParent(_gridOutLine.transform);
        gridOutLineLeft.transform.position = baseLocation;

        GameObject gridSide = new GameObject("Sprite");
        SpriteRenderer renderer = gridSide.AddComponent<SpriteRenderer>();
        renderer.sprite = _gridOutlineSprite;
        gridSide.transform.SetParent(gridOutLineLeft.transform);
        gridSide.transform.localPosition = new Vector2(gridSide.transform.localScale.x * localLocation.x, gridSide.transform.localScale.y * localLocation.y);
        gridOutLineLeft.transform.localScale = scale;
    }

    // Update is called once per frame
    void Update()
    {
        if(_playing)
        {
            _updateTimer += Time.deltaTime;
            if(_updateTimer >= (1.0f / _updateRate))
            {
                // update grid
                updateGrid();

                // increase counter
                setStepCntr(_stepCntr + 1);

                // reset timer
                _updateTimer = 0.0f;
            }
        }
        else
        {
            //reset timer
            _updateTimer = 0.0f;
        }
    }

    /// <summary>
    /// Sets step counter and updates step counter text
    /// </summary>
    /// <param name="val">New value for step counter</param>
    private void setStepCntr(int val)
    {
        _stepCntr = val;
        if(_stepCounterText != null)
        {
            _stepCounterText.text = _stepCntr.ToString();
        }
    }

    /// <summary>
    /// Handles setting of play state
    /// </summary>
    /// <param name="newState"></param>
    private void setPlayState(bool newState)
    {
        if(!_playing && newState)
        {
            //save state of current grid
            for (int x = 0; x < _blockGrid.GetLength(0); x++)
            {
                for (int y = 0; y < _blockGrid.GetLength(1); y++)
                {
                    _blockGridBackup[x, y] = _blockGrid[x, y].CurrentState;
                }
            }
        }
        _playing = newState;

        if (_stopStartButton != null)
        {
            ColorBlock newButtonColor = _stopStartButton.colors;
            string buttonText = "Start";
            if (_playing)
            {
                // set button to stop
                newButtonColor.normalColor = _stopButtonColor;
                newButtonColor.highlightedColor = _stopButtonColor;
                newButtonColor.selectedColor = _stopButtonColor;
                buttonText = "Stop";
            }
            else
            {
                // set button to play
                newButtonColor.normalColor = _startButtonColor;
                newButtonColor.highlightedColor = _startButtonColor;
                newButtonColor.selectedColor = _startButtonColor;
                buttonText = "Start";
            }

            _stopStartButton.GetComponentInChildren<Text>().text = buttonText;
            _stopStartButton.colors = newButtonColor;
        }
    }

    /// <summary>
    /// Handler for when a block is clicked on
    /// </summary>
    private void BlockClickHandler(Block sender)
    {
        sender.Alive = !sender.Alive;
        sender.CurrentColor = _selectedColor;
    }

    /// <summary>
    /// Go through grid of blocks and update according to game of life rules
    /// </summary>
    private void updateGrid()
    {
        for(int x = 0; x < _blockGrid.GetLength(0); x++)
        {
            for(int y = 0; y < _blockGrid.GetLength(1); y++)
            {
                Block blk = _blockGrid[x, y];
                Block[] neighbours = getNeighbours(x,y);
                int neighbourCnt = neighbours.Length;
                if (blk.Alive)
                {
                    if((neighbourCnt != 2) && (neighbourCnt != 3))
                    {
                        blk.Alive = false;
                    }
                }
                else
                {
                    if(neighbourCnt == 3)
                    {
                        blk.Alive = true;
                        // get new colour
                        blk.CurrentColor = calculateNewColor(neighbours);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Get neighbour blocks of block at this x y co-ordinates
    /// </summary>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private Block[] getNeighbours(int x, int y)
    {
        List<Block> neighbours = new List<Block>();
        // check blocks to the left
        if (x > 1)
        {
            if (_blockGrid[x - 1, y].Alive)
            {
                neighbours.Add(_blockGrid[x - 1, y]);
            }

            // check block to the top left
            if (y > 1)
            {
                if (_blockGrid[x - 1, y - 1].Alive)
                {
                    neighbours.Add(_blockGrid[x - 1, y - 1]);
                }
            }

            // check block to the bottom left
            if (y < (_blockGrid.GetLength(1) - 1))
            {
                if (_blockGrid[x - 1, y + 1].Alive)
                {
                    neighbours.Add(_blockGrid[x - 1, y + 1]);
                }
            }
        }

        // check blocks to the right
        if (x < (_blockGrid.GetLength(0) - 1))
        {
            if (_blockGrid[x + 1, y].Alive)
            {
                neighbours.Add(_blockGrid[x + 1, y]);
            }

            // check block to the top right
            if (y > 1)
            {
                if (_blockGrid[x + 1, y - 1].Alive)
                {
                    neighbours.Add(_blockGrid[x + 1, y - 1]);
                }
            }

            // check block to the bottom right
            if (y < (_blockGrid.GetLength(1) - 1))
            {
                if (_blockGrid[x + 1, y + 1].Alive)
                {
                    neighbours.Add(_blockGrid[x + 1, y + 1]);
                }
            }
        }

        // check block to the top
        if (y > 1)
        {
            if (_blockGrid[x, y - 1].Alive)
            {
                neighbours.Add(_blockGrid[x, y - 1]);
            }
        }

        // check block to the bottom
        if (y < (_blockGrid.GetLength(1) - 1))
        {
            if (_blockGrid[x, y + 1].Alive)
            {
                neighbours.Add(_blockGrid[x, y + 1]);
            }
        }

        return neighbours.ToArray();
    }

    Color calculateNewColor(Block[] neighbours)
    {
        Color newColor = Color.black;
        foreach(Block blk in neighbours)
        {
            newColor += blk.CurrentColor;
        }
        return newColor / neighbours.Length;
    }

    /// <summary>
    /// Clears grid of blocks
    /// </summary>
    private void clearGrid()
    {
        foreach(Block blk in _blockGrid)
        {
            blk.Alive = false;
        }
    }

    /// <summary>
    /// Resets grid to last saved grid state
    /// </summary>
    private void resetGrid()
    {
        // recover state of current grid
        for (int x = 0; x < _blockGrid.GetLength(0); x++)
        {
            for (int y = 0; y < _blockGrid.GetLength(1); y++)
            {
                _blockGrid[x, y].CurrentState = _blockGridBackup[x, y];
            }
        }

        // reset step count
        setStepCntr(0);

        // stop play
        setPlayState(false);
    }

}
