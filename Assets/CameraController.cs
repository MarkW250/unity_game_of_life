﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    /* Editor exposed properties */
    [Tooltip("Maximum camera 'size'")]
    [SerializeField]
    private int _maxCamSize = 50;

    [Tooltip("Minimum camera 'size'")]
    [SerializeField]
    private int _minCamSize = 1;

    [Tooltip("Zoom Sensitivity")]
    [SerializeField]
    private int _zoomSensitivity = 10;

    /* My objects */
    private Camera _myCamera;

    /* Prtivate state variables */
    bool _panning = false;
    Vector3 _mousePosition;

    // Start is called before the first frame update
    void Start()
    {
        _myCamera = GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        // zoom in/out
        float mouseScroll = Input.GetAxis("Zoom") * _zoomSensitivity;
        float camSize = _myCamera.orthographicSize - mouseScroll;
        if(camSize > _maxCamSize) { camSize = _maxCamSize; }
        if(camSize < _minCamSize) { camSize = _minCamSize; }
        _myCamera.orthographicSize = camSize;

        // pan
        if(Input.GetButtonDown("Pan"))
        {
            _panning = true;
            _mousePosition = _myCamera.ScreenToWorldPoint(Input.mousePosition);
        }
        if (Input.GetButtonUp("Pan"))
        {
            _panning = false;
        }

        if (_panning)
        {
            float panX = Input.GetAxis("Mouse X");
            float panY = Input.GetAxis("Mouse Y");

            Vector3 mouseChange = _myCamera.ScreenToWorldPoint(Input.mousePosition) - _mousePosition;
            Vector3 newPosition = transform.position - mouseChange;
            transform.position = newPosition;
            _mousePosition = _myCamera.ScreenToWorldPoint(Input.mousePosition);
        }

    }
}
