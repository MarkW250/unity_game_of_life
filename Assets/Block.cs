﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


// all variables to remember the state of a block
public struct BlockState
{
    public BlockState(Color initColor)
    {
        AliveState = false;
        CurrentColor = initColor;
    }
    public bool AliveState;
    public Color CurrentColor;
}

public class Block : MonoBehaviour
{
    /* Events */
    public delegate void OnClickedHandler(Block sender);
    public event OnClickedHandler OnClicked;

    /* Private objects */
    SpriteRenderer _mySpriteRendererObject = null;

    /* Private state variables */
    bool _delayedState = false;
    bool _updateState = false;
    private BlockState _blockState;

    /* Public interfaces */
    /// <summary>
    /// Gets/sets alive state.
    /// NOTE: When setting, it does not set the state immediately but rather sets it to be updated next frame.
    /// </summary>
    public bool Alive
    {
        get { return _blockState.AliveState; }
        set 
        { 
            _delayedState = value;
            _updateState = true;
        }
    }

    public BlockState CurrentState
    {
        get { return _blockState; }
        set { _blockState = value; }
    }

    public Color CurrentColor
    {
        get { return _blockState.CurrentColor; }
        set { _blockState.CurrentColor = value; }
    }

    // Start is called before the first frame update
    void Start()
    {
        Transform blockSprite = transform.Find("BlockSprite");
        if (blockSprite != null)
        {
            _mySpriteRendererObject = blockSprite.gameObject.GetComponent<SpriteRenderer>();
        }
        else
        {
            Debug.LogError(gameObject.name + ": I don't havea BlockSprite child object!");
        }

        _blockState = new BlockState(Color.white);

        _updateState = false;
    }

    // Update is called once per frame
    void Update()
    {
        // update state
        if (_updateState)
        {
            _blockState.AliveState = _delayedState;
            _updateState = false;
        }
        _mySpriteRendererObject.gameObject.SetActive(_blockState.AliveState);

        // update color
        _mySpriteRendererObject.color = _blockState.CurrentColor;
    }

    private void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            OnClicked?.Invoke(this);
        }
    }
}
